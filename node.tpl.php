<div class="post-meta">
  <?php if ($submitted) { print '<span class="submitted">' . $submitted . '</span>'; } ?>
  <?php if ($taxonomy) { print '<div class="terms">' . $terms . '</div>'; } ?>
  <?php if ($links) { print '<div class="post-tags">' . $links . '</div>'; } ?>
</div>
<div class="post-main <?php print $zebra ?>">
  <div id="node-<?php print $node->nid; ?>" class="node<?php if ($sticky) { print ' sticky'; } ?><?php if (!$status) { print ' node-unpublished'; } ?>">
    <?php print $picture ?>
    <?php if ($page == 0) { print '<h2><a class="post-title" href="' . $node_url . '" title="' . $title . '">' . $title . '</a></h2>'; } ?>
    <div class="post-entry">
      <?php print $content ?>
    </div>
    <div class="clear-block clear"></div>
  </div>
</div>
