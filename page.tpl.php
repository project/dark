<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php print $language->language; ?>" lang="<?php print $language->language; ?>">
<head profile="http://gmpg.org/xfn/11">
  <title><?php print $head_title ?></title>
  <?php print $head ?>
  <?php print $styles ?>
  <?php if ($logo) { ?>
    <style type="text/css">
    #wrapper #header h1 { background-image: url(<?php print $logo; ?>); background-position: left center; }
    </style>
  <?php } ?>
  <?php print $scripts ?>
</head>
<body class="<?php print $body_classes; ?>">
  <div id="wrapper">
    <div id="header">
	   <?php if ($search_box) { print '<div class="search">' . $search_box .'</div>'; } ?>
	   <?php if (isset($primary_links)) {
   	     print theme('links', $primary_links, array('class' => 'navmenu primary-links'));
	   } ?>
	   <?php if (isset($secondary_links)) {
	     print theme('links', $secondary_links, array('class' => 'navmenu secondary-links'));
	   } ?>
	   <h1>
		  <a href="<?php print base_path() ?>" title="<?php print $site_slogan ? $site_slogan : $site_name ?>"><?php print $site_slogan ? $site_slogan : $site_name ?></a>
	   </h1>
    </div>
    <div id="content">
      <div class="post-main">
        <?php if ($breadcrumb) { print $breadcrumb; } ?>
        <?php if ($mission) { print '<div id="mission">'. $mission .'</div>'; } ?>
        <?php if ($tabs) { print '<div id="tabs-wrapper" class="clear-block">'; } ?>
        <?php if ($title) { print '<h2'. ($tabs ? ' class="with-tabs"' : '') .'>'. $title .'</h2>'; } ?>
        <?php if ($tabs) { print $tabs .'</div>'; } ?>
        <?php if (isset($tabs2)) { print $tabs2; } ?>
        <?php if ($help) { print $help; } ?>
        <?php if ($show_messages && $messages) { print $messages; } ?>
      </div>
      <?php if ((strlen($content) > strlen('<div class="post-meta">')) && (substr($content, 0, strlen('<div class="post-meta">')) == '<div class="post-meta">')) {
        print $content;
      } else { 
        print '<div class="post-main">' . $content . '</div>'; 
      } ?>
	  <div class="clear-block clear"></div>
    </div>
    <div id="sidebar">
      <div id="sidebar1">
        <div class="sb1">
          <?php if ($left) { ?>
            <div id="sidebar-left" class="sidebar">
              <?php print $left ?>
            </div>
          <?php } ?>
        </div>
      </div>
      <div id="sidebar2">
        <div class="sb2">
          <?php if ($right) { ?>
            <div id="sidebar-right" class="sidebar">
              <?php print $right ?>
            </div>
          <?php } ?>
        </div>
      </div>
    </div>   
    <div id="bottom">
      <div class="bottom">
        <?php if ($footer_message) { print $footer_message; } ?>
        <?php if ($footer) { print $footer; } ?>
      </div>
    </div>
    <div id="footer">
      <p>
        &copy; 2007
        <a href="<?php print base_path() ?>" class="<?php print $site_name ?>" title="<?php print $site_name ?>"><?php print $site_name ?></a>&nbsp;|&nbsp;
		  <a href="http://www.ilemoned.com/wordpress/wptheme-dark" title="WordPress theme: Dark">the "dark" theme</a><br/>
		  <a href="http://validator.w3.org/check?uri=<?php print theme('hostname'); ?>" class="w3c" title="<?php print t('Valid XHTML 1.0 Strict'); ?>">XHTML</a>
		  <a href="http://jigsaw.w3.org/css-validator/validator?uri=<?php print theme('hostname') . path_to_theme(); ?>/style.css" class="w3c" title="<?php print t('Valid CSS 2.0'); ?>">CSS</a>
		  <a href="http://www.bartelme.at" class="credit" title="<?php print t('Designed by Wolfgang Bartelme'); ?>">Bartelme Design</a>
		  <a href="http://drupal.org" class="credit" title="<?php print t('Drupal'); ?>">Drupal</a>
      </p>
    </div>
    <?php if ($closure) { print $closure; } ?>
  </div>
</body>
</html>