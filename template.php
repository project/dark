<?php

drupal_rebuild_theme_registry();

/**
 * Allow themable wrapping of all comments.
 */
function dark_hostname() {
  $host = $_SERVER['SERVER_NAME'];
  $port = $_SERVER['SERVER_PORT']=='80' ? '' : ':'.$_SERVER['SERVER_PORT'];
  return 'http://' . $host . $port . '/';
}

function dark_node_submitted($node) {
  return t('<span class="cmt-author">!username</span><span class="cmt-date">!datetime</span>',
    array(
      '!username' => theme('username', $node),
      '!datetime' => format_date($node->created, 'custom', 'M d,Y')
    )
  );
}

function dark_comment_submitted($comment) {
  return t('<span class="cmt-author">!username</span><span class="post-date">!datetime</span>', 
    array(
      '!username' => theme('username', $comment), 
      '!datetime' => format_date($comment->timestamp, 'custom', 'M d,Y')
    )
  );
}

/**
 * Format an individual feed item for display on the aggregator page.
 *
 * @ingroup themeable
 */
function dark_aggregator_page_item($item) {

  $source = '';
  if ($item->ftitle && $item->fid) {
    $source = l($item->ftitle, "aggregator/sources/$item->fid", array('class' => 'feed-item-source'));
  }

  if (date('Ymd', $item->timestamp) == date('Ymd')) {
    $source_date = t('%ago ago', array('%ago' => format_interval(time() - $item->timestamp)));
  }
  else {
    $source_date = format_date($item->timestamp, 'custom', 'M d, Y');
  }

  $output .= "<div class=\"feed-item\">\n";
  $output .= "<div class=\"feed-item-meta\">$source <span class=\"feed-item-date\">$source_date</span></div>\n";
  $output .= '<h3 class="feed-item-title"><a href="'. check_url($item->link) .'">'. check_plain($item->title) ."</a></h3>\n";

  if ($item->description) {
    $output .= '<div class="feed-item-body">'. aggregator_filter_xss($item->description) ."</div>\n";
  }

  $result = db_query('SELECT c.title, c.cid FROM {aggregator_category_item} ci LEFT JOIN {aggregator_category} c ON ci.cid = c.cid WHERE ci.ii
d = %d ORDER BY c.title', $item->iid);
  $categories = array();
  while ($category = db_fetch_object($result)) {
    $categories[] = l($category->title, 'aggregator/categories/'. $category->cid);
  }
  if ($categories) {
    $output .= '<div class="feed-item-categories">'. t('Categories') .': '. implode(', ', $categories) ."</div>\n";
  }

  $output .= "</div>\n";

  return $output;
}

/**
 * Format the forum listing.
 *
 * @ingroup themeable
 */
function dark_forum_list($forums, $parents, $tid) {
  global $user;

  if ($forums) {

    $header = array(t('Forum'), t('Topics'), t('Posts'), t('Last post'));

    foreach ($forums as $forum) {
      if ($forum->container) {
        $description  = '<div style="margin-left: '. ($forum->depth * 30) ."px;\">\n";
        $description .= ' <div class="name forum_depth' . $forum->depth . '">'. l($forum->name, "forum/$forum->tid") ."</div>\n";

        if ($forum->description) {
          $description .= ' <div class="description">'. filter_xss_admin($forum->description) ."</div>\n";
        }
        $description .= "</div>\n";

        $rows[] = array(array('data' => $description, 'class' => 'container', 'colspan' => '4'));
      }
      else {
        $new_topics = _forum_topics_unread($forum->tid, $user->uid);
        $forum->old_topics = $forum->num_topics - $new_topics;
        if (!$user->uid) {
          $new_topics = 0;
        }

        $description  = '<div style="margin-left: '. ($forum->depth * 30) ."px;\">\n";
        $description .= ' <div class="name forum_depth' . $forum->depth . '">'. l($forum->name, "forum/$forum->tid") ."</div>\n";

        if ($forum->description) {
          $description .= ' <div class="description">'. filter_xss_admin($forum->description) ."</div>\n";
        }
        $description .= "</div>\n";

        $rows[] = array(
          array('data' => $description, 'class' => 'forum'),
          array('data' => $forum->num_topics . ($new_topics ? '<br />'. l(format_plural($new_topics, '1 new', '@count new'), "forum/$forum->tid", NULL, NULL, 'new') : ''), 'class' => 'topics'),
          array('data' => $forum->num_posts, 'class' => 'posts'),
		  array('data' => $forum->last_post && $forum->last_post->timetamp? t('@time ago<br />by !author', array('@time' => format_interval(time() - $forum->last_post->timestamp), '!author' => theme('username', $forum->last_post))) : t('n/a')));
      }
    }

    return theme('table', $header, $rows);

  }

}

/**
 * Format a link to a specific query result page.
 *
 * @param $page_new
 *   The first result to display on the linked page.
 * @param $element
 *   An optional integer to distinguish between multiple pagers on one page.
 * @param $parameters
 *   An associative array of query string parameters to append to the pager link.
 * @param $attributes
 *   An associative array of HTML attributes to apply to a pager anchor tag.
 * @return
 *   An HTML string that generates the link.
 */
function dark_pager_link($text, $page_new, $element, $parameters = array(), $attributes = array()) {
  $page = isset($_GET['page']) ? $_GET['page'] : '';
  if ($new_page = implode(',', pager_load_array($page_new[$element], $element, explode(',', $page)))) {
    $parameters['page'] = $new_page;
  }

  $query = array();
  if (count($parameters)) {
    $query[] = drupal_query_string_encode($parameters, array());
  }
  $querystring = pager_get_querystring();
  if ($querystring != '') {
    $query[] = $querystring;
  }

  // Set each pager link title
  if (!isset($attributes['title'])) {
    static $titles = NULL;
    if (!isset($titles)) {
      $titles = array(
        t('first') => t('Go to first page'),
        t('previous') => t('Go to previous page'),
        t('next') => t('Go to next page'),
        t('last') => t('Go to last page'),
      );
    }
    if (isset($titles[$text])) {
      $attributes['title'] = $titles[$text];
    }
    else if (is_numeric($text)) {
      $attributes['title'] = t('Go to page @number', array('@number' => $text));
    }
  }
  return '<span class="pager_button">' . l($text, $_GET['q'], array('attributes' => $attributes, 'query' => count($query) ? implode('&', $query) : NULL)) . '</span>';
}

/**
 * Format a query pager.
 *
 * Menu callbacks that display paged query results should call theme('pager') to
 * retrieve a pager control so that users can view other results.
 *
 * @param $tags
 *   An array of labels for the controls in the pager.
 * @param $limit
 *   The number of query results to display per page.
 * @param $element
 *   An optional integer to distinguish between multiple pagers on one page.
 * @param $parameters
 *   An associative array of query string parameters to append to the pager links.
 * @return
 *   An HTML string that generates the query pager.
 *
 * @ingroup themeable
 */
function dark_pager($tags = array(), $limit = 10, $element = 0, $parameters = array(), $quantity = 9) {
  global $pager_page_array, $pager_total;

  // Calculate various markers within this pager piece:
  // Middle is used to "center" pages around the current page.
  $pager_middle = ceil($quantity / 2);
  // current is the page we are currently paged to
  $pager_current = $pager_page_array[$element] + 1;
  // first is the first page listed by this pager piece (re quantity)
  $pager_first = $pager_current - $pager_middle + 1;
  // last is the last page listed by this pager piece (re quantity)
  $pager_last = $pager_current + $quantity - $pager_middle;
  // max is the maximum page number
  $pager_max = $pager_total[$element];
  // End of marker calculations.

  // Prepare for generation loop.
  $i = $pager_first;
  if ($pager_last > $pager_max) {
    // Adjust "center" if at end of query.
    $i = $i + ($pager_max - $pager_last);
    $pager_last = $pager_max;
  }
  if ($i <= 0) {
    // Adjust "center" if at start of query.
    $pager_last = $pager_last + (1 - $i);
    $i = 1;
  }
  // End of generation loop preparation.

  $li_first = theme('pager_first', (isset($tags[0]) ? $tags[0] : t('first')), $limit, $element, $parameters);
  $li_previous = theme('pager_previous', (isset($tags[1]) ? $tags[1] : t('previous')), $limit, $element, 1, $parameters);
  $li_next = theme('pager_next', (isset($tags[3]) ? $tags[3] : t('next')), $limit, $element, 1, $parameters);
  $li_last = theme('pager_last', (isset($tags[4]) ? $tags[4] : t('last')), $limit, $element, $parameters);

  if ($pager_total[$element] > 1) {
    if ($li_first) {
      $items[] = array(
        'class' => 'pager-first',
        'data' => $li_first,
      );
    }
    if ($li_previous) {
      $items[] = array(
        'class' => 'pager-previous',
        'data' => $li_previous,
      );
    }

    // When there is more than one page, create the pager list.
    if ($i != $pager_max) {
      if ($i > 1) {
        $items[] = array(
          'class' => 'pager-ellipsis',
          'data' => '�',
        );
      }
      // Now generate the actual pager piece.
      for (; $i <= $pager_last && $i <= $pager_max; $i++) {
        if ($i < $pager_current) {
          $items[] = array(
            'class' => 'pager-item',
            'data' => theme('pager_previous', $i, $limit, $element, ($pager_current - $i), $parameters),
          );
        }
        if ($i == $pager_current) {
          $items[] = array(
            'class' => 'pager-current',
            'data' => $i,
          );
        }
        if ($i > $pager_current) {
          $items[] = array(
            'class' => 'pager-item',
            'data' => theme('pager_next', $i, $limit, $element, ($i - $pager_current), $parameters),
          );
        }
      }
      if ($i < $pager_max) {
        $items[] = array(
          'class' => 'pager-ellipsis',
          'data' => '�',
        );
      }
    }
    // End generation.
    if ($li_next) {
      $items[] = array(
        'class' => 'pager-next',
        'data' => $li_next,
      );
    }
    if ($li_last) {
      $items[] = array(
        'class' => 'pager-last',
        'data' => $li_last,
      );
    }
    return '<div class="pager">' . theme('item_list', $items, NULL, 'ul', array('class' => 'pager')) . '</div>';
  }
}

/**
 * Format a list of nearby pages with additional query results.
 *
 * @param $limit
 *   The number of query results to display per page.
 * @param $element
 *   An optional integer to distinguish between multiple pagers on one page.
 * @param $quantity
 *   The number of pages in the list.
 * @param $text
 *   A string of text to display before the page list.
 * @param $parameters
 *   An associative array of query string parameters to append to the pager links.
 * @return
 *   An HTML string that generates this piece of the query pager.
 *
 * @ingroup themeable
 */
function dark_pager_list($limit, $element = 0, $quantity = 5, $text = '', $parameters = array()) {
  global $pager_page_array, $pager_total;

  $output = '<span class="pager-list">';
  // Calculate various markers within this pager piece:
  // Middle is used to "center" pages around the current page.
  $pager_middle = ceil($quantity / 2);
  // current is the page we are currently paged to
  $pager_current = $pager_page_array[$element] + 1;
  // first is the first page listed by this pager piece (re quantity)
  $pager_first = $pager_current - $pager_middle + 1;
  // last is the last page listed by this pager piece (re quantity)
  $pager_last = $pager_current + $quantity - $pager_middle;
  // max is the maximum page number
  $pager_max = $pager_total[$element];
  // End of marker calculations.

  // Prepare for generation loop.
  $i = $pager_first;
  if ($pager_last > $pager_max) {
    // Adjust "center" if at end of query.
    $i = $i + ($pager_max - $pager_last);
    $pager_last = $pager_max;
  }
  if ($i <= 0) {
    // Adjust "center" if at start of query.
    $pager_last = $pager_last + (1 - $i);
    $i = 1;
  }
  // End of generation loop preparation.

  // When there is more than one page, create the pager list.
  if ($i != $pager_max) {
    $output .= $text;
    if ($i > 1) {
      $output .= '<span class="pager_button">' . '<span class="pager-ellipsis">...</span>' . '</span>';
    }

    // Now generate the actual pager piece.
    for (; ($i <= $pager_last) && ($i <= $pager_max); $i++) {
      if ($i < $pager_current) {
        $output .= theme('pager_previous', $i, $limit, $element, ($pager_current - $i), $parameters);
      }
      if ($i == $pager_current) {
        $output .= '<span class="pager_button">' . '<strong class="pager-current">'. $i .'</strong>' . '</span>';
      }
      if ($i > $pager_current) {
        $output .= theme('pager_next', $i, $limit, $element, ($i - $pager_current), $parameters);
      }
    }

    if ($i < $pager_max) {
      $output .= '<span class="pager_button">' . '<span class="pager-ellipsis">...</span>' . '</span>';
    }
  }
  $output .= '</span>';

  return $output;
}

/**
 * Theme the theme search form.
 */
function dark_search_theme_form($form) {
  return '<div id="search" class="container-inline">'. str_replace('id="edit-submit"','id="edit-search-submit"',drupal_render($form)) .'</div>';
}

?>