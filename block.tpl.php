<div id="block-<?php print $block->module .'-'. $block->delta; ?>" class="clear-block block block-<?php print $block->module ?>">
  <?php if (!empty($block->subject)) { print '<h2>' . $block->subject . '</h2>'; } ?>
  <div class="content"><?php print $block->content ?></div>
</div>