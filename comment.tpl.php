<div class="post-meta">
  <?php if ($submitted) { print '<span class="submitted">' . $submitted . '</span>'; } ?>
  <?php if ($comment->new) { print '<a id="new"></a>' . '<span class="new">' . drupal_ucfirst($new) .'</span>'; } ?>
  <?php if ($links) { print '<div class="post-tags">' . $links . '</div>'; } ?>
</div>
<div class="post-main">
  <div class="comment<?php print ($comment->new) ? ' comment-new' : ''; print (isset($comment->status) && $comment->status  == COMMENT_NOT_PUBLISHED) ? ' comment-unpublished' : ''; print ' '. $zebra; ?>">
    <?php print $picture ?>
    <?php print '<h3>' . $title . '</h3>' ?>
    <div class="post-entry">
      <?php print $content ?>
      <?php if ($signature) { print $signature; } ?>
    </div>
    <div class="clear-block clear"></div>
  </div>
</div>